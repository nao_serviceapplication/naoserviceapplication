# [Wiki](https://bitbucket.org/nao_serviceapplication/naoserviceapplication/wiki/Home) ⬅⬅⬅

---
  
# [Code Snippets](https://bitbucket.org/snippets/nao_serviceapplication/) 

---

## NAO Ressourcen

1. [Ignite Academy ROS Kurs](http://www.theconstructsim.com/construct-learn-develop-robots-using-ros/robotigniteacademy_learnros/ros-courses-library/ros-python-course/)
2. [ROS Kurs ETH Zürich](http://www.rsl.ethz.ch/education-students/lectures/ros.html)
3. [Ignite Academy YouTube](https://www.youtube.com/playlist?list=PLK0b4e05LnzZWg_7QrIQWyvSPX2WN2ncc)
4. [ROS Installation Guide](http://wiki.ros.org/melodic/Installation/Ubuntu)
5. [Ubuntu 18.04](https://www.ubuntu.com/download/desktop)

---

[Markdown syntax guide](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)

(Ältere Versionen und default README unter history vom README file) 

